package by.home.hometask3

import android.Manifest
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView

class ContactsFragment : Fragment() {
    private val contacts: MutableList<Contact> = mutableListOf()
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        checkPermission()
        return inflater.inflate(R.layout.contacts_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        recyclerView = view.findViewById(R.id.recycler_view_list)
        recyclerView.adapter = ContactsAdapter(contacts, requireActivity())
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.READ_CONTACTS),
                100
            )
        } else {
            getContactList()
        }
    }

    private fun getContactList() {
        val uri: Uri = ContactsContract.Contacts.CONTENT_URI
        val sort: String = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        val cursor: Cursor = requireContext().contentResolver.query(
            uri,
            null,
            null,
            null,
            sort
        )!!

        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val contactId: String = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts._ID
                    )
                )

                val name: String = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )

                val uriPhone: Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI

                val selection: String = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =?"

                val phoneCursor: Cursor = requireContext().contentResolver.query(
                    uriPhone,
                    null,
                    selection,
                    arrayOf(contactId),
                    null
                )!!

                if (phoneCursor.moveToNext()) {
                    val number: String = phoneCursor.getString(
                        phoneCursor.getColumnIndexOrThrow(
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                        )
                    )

                    val email: String = phoneCursor.getString(
                        phoneCursor.getColumnIndexOrThrow(
                            ContactsContract.CommonDataKinds.Email.ADDRESS
                        )
                    )
                    val contact = Contact(contactId.toInt(), number, name, "LastName", email)
                    contacts.add(contact)

                    phoneCursor.close()
                }
            }
        }
        cursor.close()

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100
            && grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            getContactList()
        } else {
            Toast.makeText(requireActivity(), "Permission Denied.", Toast.LENGTH_SHORT).show()
            checkPermission()
        }
    }
}