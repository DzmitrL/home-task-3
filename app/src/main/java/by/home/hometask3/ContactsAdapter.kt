package by.home.hometask3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView

class ContactsAdapter(
    private val contacts: MutableList<Contact>,
    private val activity: FragmentActivity
    ) : RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {
    private val db = AppDatabase.getInstance(activity.applicationContext)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contacts_list_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact: Contact = contacts[position]

        holder.tvName.text = contact.firstName
        holder.tvNumber.text = contact.telephoneNumber

        holder.itemView.setOnClickListener {
            db.contactDao().insertAll(contact)

            Toast.makeText(activity, "Сохранение успешно", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.contact_name)
        val tvNumber: TextView = itemView.findViewById(R.id.contact_number)
    }
}