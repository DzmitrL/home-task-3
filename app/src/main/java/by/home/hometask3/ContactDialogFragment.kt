package by.home.hometask3

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit

class ContactDialogFragment(private val contacts: List<Contact>) : DialogFragment() {
    private val viewModel: ContactViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val numbers = arrayOfNulls<CharSequence>(contacts.size)

            var i = 0
            contacts.forEach { c ->
                run {
                    numbers[i] = c.telephoneNumber
                    i++
                }
            }

            builder.setTitle("Numbers in DB")
                .setItems(
                    numbers,
                    DialogInterface.OnClickListener { dialog, which ->
                        viewModel.selectContact(contacts[which])

                        parentFragmentManager.commit {
                            setReorderingAllowed(true)
                            add<ContactDetailFragment>(R.id.fragment_container_view_dialog)
                        }

                        val sharedPref = activity?.getSharedPreferences(
                            getString(R.string.preference_file_key),
                            Context.MODE_PRIVATE
                        )
                        sharedPref?.edit()
                            ?.putString("Telephone number", contacts[which].telephoneNumber)
                            ?.apply()

                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}