package by.home.hometask3

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ContactViewModel : ViewModel() {
    private val mutableSelectedItem = MutableLiveData<Contact>()
    val selectedContact: LiveData<Contact> get() = mutableSelectedItem

    fun selectContact(contact: Contact) {
        mutableSelectedItem.value = contact
    }
}