package by.home.hometask3

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity(R.layout.activity_main), View.OnClickListener {
    private lateinit var db: AppDatabase
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val selectContactButton: Button = findViewById(R.id.select_contact_button)
        val showContactsButton: Button = findViewById(R.id.button_show_contacts)
        val showFromSFButton: Button = findViewById(R.id.button_show_from_sf)
        val showNotificationButton: Button = findViewById(R.id.button_show_notification)
        selectContactButton.setOnClickListener(this)
        showContactsButton.setOnClickListener(this)
        showFromSFButton.setOnClickListener(this)
        showNotificationButton.setOnClickListener(this)

        db = AppDatabase.getInstance(applicationContext)
        db.contactDao().clearTable()

        sharedPref = applicationContext?.getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )!!
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.select_contact_button -> {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<ContactsFragment>(R.id.fragment_container_view)
                }
            }

            R.id.button_show_contacts -> {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    supportFragmentManager.findFragmentById(R.id.fragment_container_view)?.let {
                        remove(
                            it
                        )
                    }
                }

                val contacts = db.contactDao().getAll()
                val newFragment = ContactDialogFragment(contacts)
                newFragment.show(supportFragmentManager, "dialog")

            }

            R.id.button_show_from_sf -> {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    supportFragmentManager.findFragmentById(R.id.fragment_container_view_dialog)
                        ?.let {
                            remove(
                                it
                            )
                        }
                }

                val contextView = findViewById<View>(R.id.snackbar)

                val number = sharedPref.getString("Telephone number", "Empty")

                Snackbar.make(contextView, number!!, Snackbar.LENGTH_SHORT)
                    .show()
            }

            R.id.button_show_notification -> {

                val number = sharedPref.getString("Telephone number", "Empty")

//                val builder: NotificationCompat.Builder =
//                    if (number != null) {
//                        val contact = db.contactDao().getContactByNumber(number)
//                        NotificationCompat.Builder(this, channel)
//                            .setSmallIcon(R.drawable.ic_baseline_pan_tool_24)
//                            .setContentTitle("Notification")
//                            .setContentText(contact.firstName + " " + contact.lastName)
//                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                    } else {
//                        NotificationCompat.Builder(this, "i.apps.notifications")
//                            .setSmallIcon(R.drawable.ic_baseline_pan_tool_24)
//                            .setContentTitle("Notification")
//                            .setContentText("Empty")
//                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                    }
//
//                with(NotificationManagerCompat.from(this)) {
//                    notify(1, builder.build())
//                }
            }
        }
    }
}