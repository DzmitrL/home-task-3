package by.home.hometask3

import androidx.room.*

@Dao
interface ContactDao {
    @Query("SELECT * FROM contacts")
    fun getAll(): List<Contact>

    @Query("SELECT * FROM contacts WHERE id IN (:contactIds)")
    fun loadAllByIds(contactIds: IntArray): List<Contact>

    @Query("SELECT * FROM contacts WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    fun findByName(first: String, last: String): Contact

    @Query("SELECT * FROM contacts WHERE telephone_number LIKE :number")
    fun getContactByNumber(number: String): Contact

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg contacts: Contact)

    @Delete
    fun delete(contact: Contact)

    @Query("DELETE FROM contacts")
    fun clearTable()
}