package by.home.hometask3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer

class ContactDetailFragment() : Fragment() {
    private val viewModel: ContactViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View = inflater.inflate(
            R.layout.contact_dialog_view,
            container,
            false)

        val firstName = rootView.findViewById<TextView>(R.id.firstname)
        val lastName = rootView.findViewById<TextView>(R.id.lastname)
        val number = rootView.findViewById<TextView>(R.id.number)
        val email = rootView.findViewById<TextView>(R.id.email)

        viewModel.selectedContact.observe(viewLifecycleOwner, Observer { contact ->
            firstName.text = contact.firstName
            lastName.text = contact.lastName
            number.text = contact.telephoneNumber
            email.text = contact.email
        })

        return rootView
    }
}